import React, {Component} from 'react';
import {Grid, Cell} from 'react-mdl';


class Aboutme extends Component{
    render(){
        return(
            <div style={{width:'100%', margin:'auto'}}>
                <Grid className="landing-grid">
                    <Cell col={12}>
                        <img src="https://avatars0.githubusercontent.com/u/7531450?s=460&v=4" 
                        className="avatar-img"
                        alt="avatar"/>
                       <br/>
                        <div className="banner-text">
                            <h1>About Me</h1>
                            
                            <p style={{alignContent: 'center', margin: 'auto'}}>Hallo My name is Deni Soemarno. commonly called is densu or denis.
                            I was born in Bekasi, 18 December 1987. My Hobby is Read a book, and playing Soccer. I'm hardworker, i'll do my best for my jobs</p>
                            
                            <p>HTML/CSS/JS | Bootstrap | Hardware Server | Linux/Unix/Windows Server | Network |  </p>
                            <br/>
                            <div className="sosial-links">
                                {/* LinkedIn */}
                            <a href="https://www.linkedin.com/in/deni-soemarno-85aa5772/" target="_blank" rel="noopener noreferrer">
                                <i className="fa fa-linkedin-square" aria-hidden="true"/>
                            </a>

                             {/* Gitlab */}
                             <a href="https://gitlab.com/Densu87" target="_blank" rel="noopener noreferrer">
                                <i className="fa fa-gitlab" aria-hidden="true"/>
                            </a>
                            </div>
                        </div>
                    </Cell>
                </Grid>
            </div>
        );
    }
}

export default Aboutme;
