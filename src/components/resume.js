import React, {Component} from 'react';
import { Grid, Cell } from 'react-mdl';
import Education from './education';
import Experience from './experience';
import Skill from './skill';



class Resume extends Component{
    render(){
        return(
            <div>
               <Grid>
                   <Cell className="resume-left-col" col={4}>
                    <div style={{textAlign: 'center'}}>
                        <img className="avatar"
                        src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxAQChAQEBAJEBAJDRYNDQkJDQ8IFQgKIB0iIiAdHx8kKDQkJCYxJx8fITItMSs3MDAwIx8/ODM4NyouLi0BCgoKDg0OFhAQGC8dGCArMi0tMi0rLS0rLTctKystMDcuLjc3NzguNzc3NyszLS0rKysrKzcrKysrKysrKysrLf/AABEIAMgAyAMBIgACEQEDEQH/xAAcAAEAAQUBAQAAAAAAAAAAAAAAAwECBAUGBwj/xAA/EAABAwIDBAcHAwEGBwAAAAABAAIDBBESITEFQVFxBhMiYYGh8AcyQlKRscEUI9EzJERTcoLxFTRDYmNk4f/EABsBAQACAwEBAAAAAAAAAAAAAAABAgMEBQYH/8QAKREAAgIBAwQCAgEFAAAAAAAAAAECAxEEITEFEkFREyIyYYEGJDNCcf/aAAwDAQACEQMRAD8A4ZERcU+nhERAEREAREQBERAERDoSdBqTlZSk3wVlOMF9ngIrcYte7bcbhXAjcQpcJLlFI31y4kmERFUyhERAEREAREQBERAEREAREQBERAEREAREQBSQwlxsLcycIAVjRnuHedypJM5zgyO+Bmpw3648lnop+R78HM6lr1pofXeT4MyOOJrQXuaQfldjv3WCwK2sc7sx4AGZDACLhb/ZfR6pkj/ozt6yxMklmGQ/dbGPoXM1923xHKwGniuiuyCxE8fdbffLum8nn76OQjKPPXGxpIKwZI5GnSQG97WIzXslH0IIcHONnN/wgG4R+Vr9vdFZMBdEy9j8xj8clHyRezKKE47pnmtLtQghsg7sfukc1tgbi4Nwd4zuFbtLYsrgf2rZajcea0dNO6GXC7Fh0LTlYrXt06lvE7eg6vOtqF269m/RWseHNuCCDvGd1ctBprZnqoyUllBERCQiIgCIiAIiIAiIgCIiAIiIAiK6IDEL6Xz3XCJZeCs5dsWyx7iLAWJflhXo3Qno8yOISSBr3ydqzgLNK8/omF9UywuMVrn4eP8AC9g2b2Ymjg3zXTkvjgoo8LZbLUXSnI28fruWSHNHDyF1iwMLs1LJREm/DyWFZJeCdwFtyxZMNiMv5U4hdayhfR2TBGxqqiijNzhbnqLaleYdOujLGkyxiwdq23uuXq9VGWhcx0kg6ymeBrbK3FWhJxe5SyKkjyTZDsIcx173uL/EFsVAWDrg4Wu3sluhLlOsOqhieV5PTdFvdlHa/wDXYIiLWOyEREAREQBERAEREAREQBERAFczUc/JWrK2ZSmaoawB5vdxEdicAFzZTHkpa0oSb4Nl0Lputr3E+5TDEG/+Uneuvrq6d04hgbk335ndnwCxOhuzGx1FSGnE0vbgeRhJaRfPvzW42oZYnHqo8brbrMsuhKW54Xt5LAza2DGHwtA0Y3C4LL2dtapFhORi0dhta/FaCav2uZmxshg6qTC59QA54p2775rcUNJO97ut6ssj92VmXWDluUybwRGKbN87a5DdQucr5KuokHVyuY3eGnDcLe1MEYpxYZ3zPELTVNHMJAGvYyN4v1nvknhZUTZaUVgwqrYdYI86t17ZML3PsFrKJ9QybqpnCRpHYm3h3AqLqdsF7+tMAbE2zPceKh19bjTJbXYtJK83ma0FuYscYLlabZWCXJ590qphFWONrNls+wys+6gXV9MqJhrIC8OLSSMDf+o7UD6rU7f2Y+B8ZeGA1EePDEMDWuvoFh1DzGJ2eizUbJxfk1SIi1D0oREQBERAEREAREQBERAEREAW96EVIj2tCXWtLeIk52JGXmtEr4ZSx7XjIxuDh3EG6lPDMd0O+uUfZ61TRCOvmIy64tfwzst9S0wee1nfxXN/qg6qa8aTwte3xzXRbOqRl/tmtxbnipJptGbLsmPeX2+UHVY20cMbGsYAAdw3lZ8s12+jmuc6RVMzHNMUDp3OFmgOEQY7vJ0V36McU/JmVEZ6m9v/AKptkYJoSx4BDTvtkVy9bt+qMQjZS1Bm06h9mNjdxLtLLZdFqmZzn9dF1JsB7weJJO471CWC0t0dC3Y8Tb5fXctXXQhulhbRbl9SAzkue2nVZ+fDJTIrHPk5ius7akAdYhry7kLFaT2gVOKriYP7vTMvvs45/wALNLjJtVgvpfwXO9Jpce0pyCCA/CCM+yAB+Fguf1wdjpNebs+kaxERax6UIiIAiIgCIiAIiIAiIgCIiAIiIDrei21HSOjifrTtwsfxj3DwXdQXuPscl5Jsmp6uoa69t3ivUNm17ZIgQc9471tVPMTy3U6fjuyuGbaSuwNz9Fa+XarBnJI0F2YY0i5CllgbK2x366jJax+xYopC5kLCDmcusJ+qyo53knm6Q04bbESSfdy+617+kMIsWSsa4H+nKQ3EO5bD9a+9mUstvma0Muoo9mB8gdLGzXEBI0PIcpeA8eDZUdeZI7i9vusOuF7+rLOlc1jbCwH0uVzm3NsNiicbi7sh3FU5IOV2nXOiqXlhs6xYHj4brRE+uJUk8pc651cblRrWteZYPV9NqUKU/LCIixnQCIiAIiIAiIgCIiAIiIAiIgCIiBvHIB9cFv8AZW0HC1jzGlyueLwNSFtehezn1u0XwsfgPUOcy+jni2q2aK5N/o4XWL6OxJv7HcbK22A4NeS2/ul2jiukp6luRJHcclw9Xsipgd1c8ZbuxHSQdxVWmVjbBzgN2POy2OxnnlNeGeiu2hHhtlzFgtXWVzbE3AtmuBkmmz/tBHdkoS6VzbGQkcdLhT25Hdg2e2ukWZbH2rfFwXHV9Q57sTzybwW3/Q3cGM7T5DhYxgLzI7uCy+k3Qp9JsiSoqHfvvtgp2kO/TtuMyeKsq2+Cvyxi02csiwKXaLS0B+IOGRIBIKzWPBGRBXPnXJPdHtNNq6LIRUJL/hciIsZuBERAEREAREQBERAEVjpmjVzcu+9ioHVnAE88lkjTOXCNO7X6er8pGUhNhnlzyWGZnu0sOWaoIt5JPPNbMNHJ8s5V/X64/wCOOSZ9SPhzPHQLHklcdSeQ7KvsrQ255ea24aeEPBwtT1TUXvd4XpFjm2ZfefJdP7LKzqekFMd07zCf9QIXN1ANlJseqMNXDKNaeZkn0cCs2Ec5tvk+rp4GSRlr2se12rHgPBXC9JuhjGtMkM4iaTYwVD8LQ46YSfsV3jHAtBGjxcHiF5X7W6yudPG6jxGPY37s+ACT99w1tvsPuqJZ2JjJrc01V0JmYC+Q1Ab87bOB8Qp9j9DpJ3YWOfhHvSON8LVx8HTfaD8v1dSA7Lq2kMaBwtZIOmte2riEMtQ91PKC2MnsyHeCOBUfF+zL87xjB730b6K01ELsbilIs6qk7Tj3DgFxvtwmw7Na3fPM1o5C5/hehbJ2gJ6eOQAt61gLozmYn7x4HJeTe3+q/cpIuDHzEcyAPsVaGzMDeTxm3auNRn4rMiebA53PhmseNvaWWG28furYLKTjujKiqTaxz79FOJ28bc1iNCrgWvPSwludXT9Z1FSw3lfszgfWqLBAIORt+VKKk7wD5LVno5L8dztafr1UtrFgyUUTahp3258VKD61WtKuUeUderVVW/hJMIiKhsBRzHs8/IKRRyDM335DktjTQ7pnL6vqHTp3jl7GJHBv+bPkpxDwU0LLNHrNXkLrpHhXJvkgDELVNhVHBSVMdw9cEY3zKlczK/q6o4W+luZUAgmUJ9d6yJAoLXN+H3QH010R2mJNg0s7vhpRj33c0WP2VuyNl4oXvmYC6scZHtdoLrkPZLWPqNmNpcJ6uiqHOkkvk9h7TW/W67bpltP9Jsipn0MUJDDpaU5D7qvDB879Jaen/wCKVQpg5sTZS2MX0sbG30K7z2K9GoJY5qt/afDMYWREAhgsDfzXnYsQSDe+WL5ivSfYPtIB9XSn3pC2pZna7fdP4V5bEHqUNP1cpw5NkOIAZAO3rwL2x7SE+3pWg3bRtbTi2dnAXPmV9BVtmQukcf8Al2mS5yDbC6+VNqTumnlmdm6eV0ru9xJKonnck18Y7SzrXbyWJG3tLOYN3FSCjQpWtyVjBu4ZKcKQRlqoGK9w9aoNEBA5iugdZxHfbwUls/RWNKbYiON+YVLI90WjY01sq7YyXs2CIDki4b5Po0fskwosQNzbT4juClUXVX10b8I+Jy6GiXLPM/1FY/pD+SSE9nx8lIo4Rkef0Utsl0Dy5aVYfurrKjtOSAtkvbLDcH4rkEKFxcMy082doLIVCgMYEOGR5ngFSUAZDd5qcKCYKAerewep7VZFxEcoH1B/C3ntvqMOwmsBt+oq2NPe0An8BcP7FqrBtwM3VNPIzhdwsfwV0Xt7qP7PRxfNJJKRyAA+5VcfYHlGzh2LHeclv/Z7VGn6S0jr2E8pgduuHgj72WmohZjVdHP1dbBKMjDURvvws4LI+CPJ9B+0St6no/WPBsTD1Q/zOIb+V80Be7+2ysDdiRsH97qmDmwAu/heFNGaxx4JIwyzgskvAOZGXmrC25z8slLFG0bhfjqrAtY65vnYjUjDcqUOvx/lHeu9Wj3uTfqSpBIAqKnrwVHFAVH28FiTZi3nosrFksaZ9h6zUMlPDyZdM/EwcRke4qqxdmOzfwuD4ouHfHtm0fQen3u3Twk/RnX9d6sJKutchWuIubegurpYpVo8j1e52amWfGxdTnMqe2XrNY0Du1zCyty2TlkJGaoQrnIPXeoBbuVpKqN/cqFAWgKKZTjRRShAbnoBWdTt2jf/AOwGO/yu7J+66r24VGLacMf+BS3I4OLj/C8+oJjHURyDWKRrx3EG66v2p1Al6QPIzBihtb5SwH8qFyDnoGdkDgFi7RyseDr8lsQ3PTRa/a47IA+JWZB6P7bKy8OzoxvhdOfENAXlzF03T3awqqilIIIg2bAw2ztIW3d5my5oBURJUaqVhVgCnZNYWAYCB71r3UgjKo3U81diubnXjorYz2b8c1ILio3nJX+uahkd2rKAXYliVRzsP91kA5nwUNQMr8EBJst3acOIuihoXWmb/wB3ZRc7UV5m2ex6PqP7ZL0zbG9u85eCteABbipbLHnct6qPbBJnmNbarb5zXDZbCbPGqz2aegtRJIQ0kbs1tYz5+YWVGqWPGatspJQoVAG/mrb5q558vsqIC4Kx4V6scgIQM1sNoVv6mvEmZPVxsdf5msa0/ZYIBupKJv8AaD9fFAbXD64rB2jHcA8FsL+u9RTR9gjxVippnNVWhVcM1UDJVLABLZK9vrvQty9BARP90/Qc1UDySUZtH+o8lQkIA42uTuWJju/1or6mQYSLnw3hYglub2sGj6qAZkXuk8XfVRznJXRf0294v4qOfRAQMNng/K4FFR6qscoJs2atRKtYR0MhsFhSlEWU1jHeMiOIss/Z8mKFhOuGx5jJERAyJtFAAiKQCFTdyRFAGL1xVCiIC0qejH73gERSDaNGajq8o3cvqiKSppjqiIqli9ivByCIgMZ7ruJGnujkFG7mPuiKAYdWDZYt+zb5j9VVFBJtHgAADc1QyDsoiAxh7qIiA//Z"
                        alt="avatar"
                        style={{height: '200px'}}/>
                    </div>
                    <h2 style={{paddingTop: '2px',textAlign: 'center'}}>Deni Soemarno</h2>
                    <h4 style={{color: 'grey', textAlign: 'center'}}>Devops Engineer</h4>
                    <hr style={{borderTop: '3px solid #833ffb2'}}></hr>
                    <p style={{alignContent: 'center',margin: 'auto'}}>Hallo My name is Deni Soemarno. commonly called is densu or denis.
                            I was born in Bekasi, 18 December 1987. My Hobby is Read a book, and playing Soccer. I'm hardworker, i'll do my best for my jobs</p>
                    <hr style={{borderTop: '3px solid #833ffb2'}}></hr>
                    <h2 style={{paddingTop: '2px',textAlign: 'center'}}>MOTO</h2>
                    <p style={{fontFamily:'Anton',textAlign:'center'}}>"Work hard in silence, let your success be your noise."</p>
                    <p style={{textAlign: 'right'}}>Quote By "Frank Ocean"</p>
                    <hr style={{borderTop: '3px solid #833ffb2'}}></hr>
                    <h5><i className="fa fa-home" aria-hidden="false"/> Address</h5>
                    <p>KP Duku RT14/06 Kebayoran lama Jakarta Selatan</p>
                    <h5><i className="fa fa-phone" aria-hidden="false"/> Phone</h5>
                    <p>(+62)813-8595-9558</p>
                    <h5><i className="fa fa-envelope-square" aria-hidden="false"/> Email</h5>
                    <p>denisoemarno@gmail.com</p>
                    <h5><i className="fa fa-chrome" aria-hidden="false"/> Web Personal</h5>
                    <p>https://myportfolio-densu.netlify.com/</p>
                    <hr style={{borderTop: '3px solid #833ffb2'}}></hr>
                   </Cell>
                   <Cell className="resume-right-col" col={8}>
                       <h2>Education</h2>
                       <Education
                        startYear={2010}
                        endYear={2015}
                        schoolName="STMIK ISLAM INTERNASIONAL"
                        schoolDescription="International Islamic University College(IIUC), yang dikenal sebagai STMIK ISLAM INTERNASIONAL JAKARTA adalah 
                        lembaga pendidikan swasta yang terdaftar di kopertis III Jakarta, dan didukung sepenuhnya oleh International Islamic Malaysia(IIUM) "
                       />
                       <Education
                        startYear={2002}
                        endYear={2005}
                        schoolName="SMK TARUNA TERPADU"
                        schoolDescription="SMK Taruna Terpadu, yang dikenal sebagai Borcess (Bogor Central School) adalah 
                        lembaga pendidikan swasta yang mempunyai akreditasi A."
                       />
                       {/* <Education
                        startYear={1999}
                        endYear={2002}
                        schoolName="SLTP NEGERI 10 DEPOK"
                        schoolDescription="SLTP Negeri 10 Depok, yang dikenal sebagai SWG adalah 
                        pendidikan sekolah lanjutan pertama negeri yang mempunyai akreditasi A."
                       />
                       <Education
                        startYear={1993}
                        endYear={1999}
                        schoolName="SD NEGERI 1 SAWANGAN"
                        schoolDescription="SLTP Negeri 10 Depok, yang dikenal sebagai SWG adalah 
                        pendidikan sekolah lanjutan pertama negeri yang mempunyai akreditasi A."
                       /> */}
                       <hr style={{borderTop: '3px solid #e22947'}}/>
                        <h2>Experience</h2>
                        <Experience
                        startYear={2019}
                        endYear={2020}
                        jobsName="DEVOPS ENGINEER"
                        jobCompany="PT Ogya Tekno Nusantara"
                        jobsDescription="Pekerjaan yang bagaimana membangun sistem infrastruktur yang terotomasi, terintegrasi, dan fleksibel agar developer
                        dapat meluncurkan kode-kode mereka secepat dan seefisien mungkin. "
                       />
                       <Experience
                        startYear={2016}
                        endYear={2019}
                        jobsName="SYSTEM ADMINISTRATOR"
                        jobCompany="PT Multisoft Java Technology"
                        jobsDescription="System Administrator (dikenal juga sebagai admin, administrator, sysadmin, site admin, dll) 
                        merupakan profesi yang memiliki tugas untuk melakukan administrasi terhadap sistem, melakukan pemeliharaan sistem, 
                        memiliki kewenangan mengatur hak akses terhadap sistem, 
                        serta hal-hal lain yang berhubungan dengan pengaturan operasional sebuah sistem. 
                        Administrator atau system administrator ini biasanya masuk ke dalam departemen atau divisi teknologi dan informasi dalam struktur organisasi perusahaan. "
                       />
                       <Experience
                        startYear={2015}
                        endYear={2016}
                        jobsName="APPLICATION SUPPORT"
                        jobCompany="PT Pratesis"
                        jobsDescription="Pekerjaan yang mengimplementasi system yang di buat developer untuk kebutuhan customer/client
                        , memberikan pengetahuan kepada customer/client tentang penggunaan aplikasi serta memberikan report alur bisnis yang di butuhkan customer/client
                        kebutuhan system kepada developer. "
                       />
                       <Experience
                        startYear={2011}
                        endYear={2015}
                        jobsName="IT SUPPORT ENGINEER"
                        jobCompany="PT Quadra Solution"
                        jobsDescription="Pekerjaan yang mempunyai tanggung jawab untuk instalasi,evaluasi serta peningkatan terhadap 3 objek utama seperti
                         hardware,software,jaringan, serta melakukan troubleshoot terhadap hardware,software,dan jaringan di dalam suatu perusahaan. "
                       />
                       <hr style={{borderTop: '3px solid #e22947'}}/>
                       <Skill
                        skill="HTML/CSS/JS"
                        progress={75}
                       />
                       <Skill
                        skill="BOOTSTRAP"
                        progress={70}
                       />
                       <Skill
                        skill="HARDWARE SERVER"
                        progress={80}
                       />
                       <Skill
                        skill="LINUX/UNIX/WINDOWS"
                        progress={75}
                       />
                       <Skill
                        skill="NETWORKING"
                        progress={70}
                       />
                    </Cell>
               </Grid>
            </div>
        );
    }
}

export default Resume;