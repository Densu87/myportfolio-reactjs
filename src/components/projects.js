import React, {Component} from 'react';
import { Tabs, Tab, Grid, Cell, Card, CardTitle, CardText, CardActions, Button, CardMenu, IconButton } from 'react-mdl';


class Projects extends Component{
    constructor(props) {
        super(props)
        this.state = { activeTab: 0 };
    }

    toggleCategory(){
        if(this.state.activeTab === 0){
            return(
                <div className="project-grid">
                    {/*Project 1 */}
                    <Card shadow={5} style={{width: '450', margin: 'auto'}}>
                    <CardTitle style={{color: 'black', height: '176px', background: 'url(https://2.bp.blogspot.com/-IB7QJ6YBWXQ/V1sV8Ip_dDI/AAAAAAAAFA4/jgsJi8c81xU1t8Q2Q7E4TmzC9KKmyMhywCLcB/s1600/HTML5_CSS_JavaScript.png) center /cover'}}>HTML/CSS/JS</CardTitle>
                    <CardText> Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                Mauris sagittis pellentesque lacus eleifend lacinia...
                    </CardText>
                    <CardActions border>
                        <Button colored>Github</Button>
                        <Button colored>Codepen</Button>
                        <Button colored>Live Demo</Button>
                    </CardActions>
                    <CardMenu style={{color: 'fff'}}>
                        <IconButton name="share"></IconButton>
                    </CardMenu>
                </Card>
                {/*Project 2 */}
                <Card shadow={5} style={{width: '450', margin: 'auto'}}>
                    <CardTitle style={{color: 'black', height: '176px', background: 'url(https://2.bp.blogspot.com/-IB7QJ6YBWXQ/V1sV8Ip_dDI/AAAAAAAAFA4/jgsJi8c81xU1t8Q2Q7E4TmzC9KKmyMhywCLcB/s1600/HTML5_CSS_JavaScript.png) center /cover'}}>HTML/CSS/JS</CardTitle>
                    <CardText> Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                Mauris sagittis pellentesque lacus eleifend lacinia...
                    </CardText>
                    <CardActions border>
                        <Button colored>Github</Button>
                        <Button colored>Codepen</Button>
                        <Button colored>Live Demo</Button>
                    </CardActions>
                    <CardMenu style={{color: 'fff'}}>
                        <IconButton name="share"></IconButton>
                    </CardMenu>
                </Card>
                 {/*Project 3 */}
                 <Card shadow={5} style={{width: '450', margin: 'auto'}}>
                    <CardTitle style={{color: 'black', height: '176px', background: 'url(https://2.bp.blogspot.com/-IB7QJ6YBWXQ/V1sV8Ip_dDI/AAAAAAAAFA4/jgsJi8c81xU1t8Q2Q7E4TmzC9KKmyMhywCLcB/s1600/HTML5_CSS_JavaScript.png) center /cover'}}>HTML/CSS/JS</CardTitle>
                    <CardText> Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                Mauris sagittis pellentesque lacus eleifend lacinia...
                    </CardText>
                    <CardActions border>
                        <Button colored>Github</Button>
                        <Button colored>Codepen</Button>
                        <Button colored>Live Demo</Button>
                    </CardActions>
                    <CardMenu style={{color: 'fff'}}>
                        <IconButton name="share"></IconButton>
                    </CardMenu>
                </Card>
                </div>
                
            )
        }else if(this.state.activeTab === 1){
            return(
                <div className="project-grid">
                    {/*Project 1 */}
                    <Card shadow={5} style={{width: '450', margin: 'auto'}}>
                    <CardTitle style={{color: 'black', height: '176px', background: 'url(https://seeklogo.net/wp-content/uploads/2016/06/bootstrap-logo-vector-download.jpg) center /cover'}}>Bootstrap</CardTitle>
                    <CardText> Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                Mauris sagittis pellentesque lacus eleifend lacinia...
                    </CardText>
                    <CardActions border>
                        <Button colored>Github</Button>
                        <Button colored>Codepen</Button>
                        <Button colored>Live Demo</Button>
                    </CardActions>
                    <CardMenu style={{color: 'fff'}}>
                        <IconButton name="share"></IconButton>
                    </CardMenu>
                </Card>
                {/*Project 2 */}
                <Card shadow={5} style={{width: '450', margin: 'auto'}}>
                    <CardTitle style={{color: 'black', height: '176px', background: 'url(https://seeklogo.net/wp-content/uploads/2016/06/bootstrap-logo-vector-download.jpg) center /cover'}}>Bootstrap</CardTitle>
                    <CardText> Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                Mauris sagittis pellentesque lacus eleifend lacinia...
                    </CardText>
                    <CardActions border>
                        <Button colored>Github</Button>
                        <Button colored>Codepen</Button>
                        <Button colored>Live Demo</Button>
                    </CardActions>
                    <CardMenu style={{color: 'fff'}}>
                        <IconButton name="share"></IconButton>
                    </CardMenu>
                </Card>
                 {/*Project 3 */}
                 <Card shadow={5} style={{width: '450', margin: 'auto'}}>
                    <CardTitle style={{color: 'black', height: '176px', background: 'url(https://seeklogo.net/wp-content/uploads/2016/06/bootstrap-logo-vector-download.jpg) center /cover'}}>Bootstrap</CardTitle>
                    <CardText> Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                Mauris sagittis pellentesque lacus eleifend lacinia...
                    </CardText>
                    <CardActions border>
                        <Button colored>Github</Button>
                        <Button colored>Codepen</Button>
                        <Button colored>Live Demo</Button>
                    </CardActions>
                    <CardMenu style={{color: 'fff'}}>
                        <IconButton name="share"></IconButton>
                    </CardMenu>
                </Card>
                </div>
            )
        }else if(this.state.activeTab === 2){
            return(
                <div className="project-grid">
                    {/*Project 1 */}
                    <Card shadow={5} style={{width: '450', margin: 'auto'}}>
                    <CardTitle style={{color: 'black', height: '176px', background: 'url(https://macro-trend.com/image/data/Product/sever/tower%20Servers/ProLiant%20ML10%20Servers/c04587505.png) center /cover'}}>Server Tower</CardTitle>
                    <CardText> Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                Mauris sagittis pellentesque lacus eleifend lacinia...
                    </CardText>
                    <CardActions border>
                        <Button colored>Github</Button>
                        <Button colored>Codepen</Button>
                        <Button colored>Live Demo</Button>
                    </CardActions>
                    <CardMenu style={{color: 'fff'}}>
                        <IconButton name="share"></IconButton>
                    </CardMenu>
                </Card>
                {/*Project 2 */}
                <Card shadow={5} style={{width: '450', margin: 'auto'}}>
                    <CardTitle style={{color: 'black', height: '176px', background: 'url(https://assets.ext.hpe.com/is/image/hpedam/s00005332?$zoom$&.png) center /cover'}}>Server Blade</CardTitle>
                    <CardText> Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                Mauris sagittis pellentesque lacus eleifend lacinia...
                    </CardText>
                    <CardActions border>
                        <Button colored>Github</Button>
                        <Button colored>Codepen</Button>
                        <Button colored>Live Demo</Button>
                    </CardActions>
                    <CardMenu style={{color: 'fff'}}>
                        <IconButton name="share"></IconButton>
                    </CardMenu>
                </Card>
                 {/*Project 3 */}
                 <Card shadow={5} style={{width: '450', margin: 'auto'}}>
                    <CardTitle style={{color: 'black', height: '176px', background: 'url(https://i.ebayimg.com/00/s/MTIwMFgxMjAw/z/kwkAAOSwXRpbO5D-/$_57.jpg) center /cover'}}>Server Rack Mount</CardTitle>
                    <CardText> Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                Mauris sagittis pellentesque lacus eleifend lacinia...
                    </CardText>
                    <CardActions border>
                        <Button colored>Github</Button>
                        <Button colored>Codepen</Button>
                        <Button colored>Live Demo</Button>
                    </CardActions>
                    <CardMenu style={{color: 'fff'}}>
                        <IconButton name="share"></IconButton>
                    </CardMenu>
                </Card>
                </div>
            )
        }else if(this.state.activeTab === 3){
            return(
                <div className="project-grid">
                    {/*Project 1 */}
                    <Card shadow={5} style={{width: '450', margin: 'auto'}}>
                    <CardTitle style={{color: 'black', height: '176px', background: 'url(https://banner2.cleanpng.com/20180729/ss/kisspng-penguin-tux-linux-kernel-vectorlinux-python-stickers-5b5d465b9990e8.352894071532839515629.jpg) center /cover'}}>Linux</CardTitle>
                    <CardText> Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                Mauris sagittis pellentesque lacus eleifend lacinia...
                    </CardText>
                    <CardActions border>
                        <Button colored>Github</Button>
                        <Button colored>Codepen</Button>
                        <Button colored>Live Demo</Button>
                    </CardActions>
                    <CardMenu style={{color: 'fff'}}>
                        <IconButton name="share"></IconButton>
                    </CardMenu>
                </Card>
                {/*Project 2 */}
                <Card shadow={5} style={{width: '450', margin: 'auto'}}>
                    <CardTitle style={{color: 'black', height: '176px', background: 'url(https://ezwaysite.files.wordpress.com/2017/03/unix2.png?w=470&h=309) center /cover'}}>Unix</CardTitle>
                    <CardText> Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                Mauris sagittis pellentesque lacus eleifend lacinia...
                    </CardText>
                    <CardActions border>
                        <Button colored>Github</Button>
                        <Button colored>Codepen</Button>
                        <Button colored>Live Demo</Button>
                    </CardActions>
                    <CardMenu style={{color: 'fff'}}>
                        <IconButton name="share"></IconButton>
                    </CardMenu>
                </Card>
                 {/*Project 3 */}
                 <Card shadow={5} style={{width: '450', margin: 'auto'}}>
                    <CardTitle style={{color: 'black', height: '176px', background: 'url(https://www.iperiusbackup.net/wp-content/uploads/2018/10/Windows-Server-2019-Preview.png) center /cover'}}>Windows Server</CardTitle>
                    <CardText> Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                Mauris sagittis pellentesque lacus eleifend lacinia...
                    </CardText>
                    <CardActions border>
                        <Button colored>Github</Button>
                        <Button colored>Codepen</Button>
                        <Button colored>Live Demo</Button>
                    </CardActions>
                    <CardMenu style={{color: 'fff'}}>
                        <IconButton name="share"></IconButton>
                    </CardMenu>
                </Card>
                </div>
            )
        }else if(this.state.activeTab === 4){
            return(
                <div className="project-grid">
                    {/*Project 1 */}
                    <Card shadow={5} style={{width: '450', margin: 'auto'}}>
                    <CardTitle style={{color: 'black', height: '176px', background: 'url(https://i0.wp.com/tulisanit.com/wp-content/uploads/2019/06/setting-2Bmikrotik-2Bdari-2Bawal.jpg?fit=620%2C330&ssl=1) center /cover'}}>Mikrotik</CardTitle>
                    <CardText> Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                Mauris sagittis pellentesque lacus eleifend lacinia...
                    </CardText>
                    <CardActions border>
                        <Button colored>Github</Button>
                        <Button colored>Codepen</Button>
                        <Button colored>Live Demo</Button>
                    </CardActions>
                    <CardMenu style={{color: 'fff'}}>
                        <IconButton name="share"></IconButton>
                    </CardMenu>
                </Card>
                {/*Project 2 */}
                <Card shadow={5} style={{width: '450', margin: 'auto'}}>
                    <CardTitle style={{color: 'black', height: '176px', background: 'url(https://blog.sribu.com/wp-content/uploads/2015/02/cisco-logo-transparent.png) center /cover'}}>Cisco</CardTitle>
                    <CardText> Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                Mauris sagittis pellentesque lacus eleifend lacinia...
                    </CardText>
                    <CardActions border>
                        <Button colored>Github</Button>
                        <Button colored>Codepen</Button>
                        <Button colored>Live Demo</Button>
                    </CardActions>
                    <CardMenu style={{color: 'fff'}}>
                        <IconButton name="share"></IconButton>
                    </CardMenu>
                </Card>
                 {/*Project 3 */}
                 <Card shadow={5} style={{width: '450', margin: 'auto'}}>
                    <CardTitle style={{color: 'black', height: '176px', background: 'url(https://cdn2.tstatic.net/tribunnews/foto/bank/images/tp-link_20160829_114724.jpg) center /cover'}}>TP-LINK</CardTitle>
                    <CardText> Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                Mauris sagittis pellentesque lacus eleifend lacinia...
                    </CardText>
                    <CardActions border>
                        <Button colored>Github</Button>
                        <Button colored>Codepen</Button>
                        <Button colored>Live Demo</Button>
                    </CardActions>
                    <CardMenu style={{color: 'fff'}}>
                        <IconButton name="share"></IconButton>
                    </CardMenu>
                </Card>
                </div>
            )
        }
    }

    render(){
        return(
            <div className="category-tabs">
                <Tabs activeTab={this.state.activeTab} onChange={(tabId) => this.setState({ activeTab: tabId})} ripple>
                    <Tab>HTML/CSS/JS</Tab>
                    <Tab>Bootstrap</Tab>
                    <Tab>Server</Tab>
                    <Tab>Linux/Unix/Windows Server</Tab>
                    <Tab>Network</Tab>
                </Tabs>
                <section>
                    <div className="projects-grid">
                        <Grid>
                            <Cell col={12}>
                                <div className="content">{this.toggleCategory()}</div>
                            </Cell>
                        </Grid>
                    </div>
                </section>
            </div>
        );
    }
} 

export default Projects;