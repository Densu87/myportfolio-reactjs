import React,{ Component } from 'react';
import { Grid, Cell } from 'react-mdl';



class Experience extends Component {
    render(){
        return(
            <Grid>
                <Cell col={4}>
                <p>{this.props.startYear} - {this.props.endYear}</p>
                </Cell>
                <Cell col={8}>
                    <h4 style={{marginTop: '0px'}}>{this.props.jobsName}</h4>
                    <p>{this.props.jobCompany}</p>
                    <p className="description">{this.props.jobsDescription}</p>
                </Cell>
            </Grid>
        )
    }
}

export default Experience;

